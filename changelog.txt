3: 20 April 2004
 - Added right-to-left, bottom-to-top text support
 - Added -font switch
 - Added -altgd switch
 - Added long switch names
 - Rewrote/tidied various parts
 - Made explictly BSD licenced.
 
2: 27 June 2003
 - Support for page sizes other than whatever default your Postscript interpreter chooses.
 - User-definable margins around the page
 - Smaller PS code output (still honking big mind)
 - Skips pixels that are transparent
 - Optional backing rectangle of user defined colour
 - Margin between backing rectangle and text user definable
 - Support for title field in the Postscript file
 - Output can be to a file, rather than just STDOUT
 - All options can be specified on the command line

1:
 - Initial release
