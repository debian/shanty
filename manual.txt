Shanty v3
Written by Duncan Martin
http://www.codebunny.org/coding/shanty/

ABOUT
Shanty takes a text file and an image (PNG or JPG) and creates a PostScript
file where one pixel in the image becomes one character in the PostScript.

LICENCE
Share and enjoy - no warranties.  See source or licence.txt for details.

USE

shanty  -i image_file [-t text_file] [-o output_file] [-s paper_size]
        [-d density] [-m margin] [-b background_colour [-x padding]]
        [-n title] [-l orientation] [-rtl] [-btt] [-f font_name] [-altgd]


Note: under Windows use "perl shanty.pl" then the options.

SWITCHES
-i           : Name of the image to load in. JPG and PNG images are supported.
-image       : This is the only compulsory field.

-t           : Name of the text file to load in, if omitted STDIN is used.
-text        : STDIN is usually the keyboard.

-o           : Name of the PostScript file to produce, if omitted STDOUT is
-output      : used.  STDOUT is usually the screen.

-s           : Size of the paper to work with.  This field should be one of:
-size        : a0, a2, a3, a4, a5, a6, letter, broadsheet, ledger, tabloid,
             : legal, executive and 36x36. Default is "a4".
     
-d           : Density of the text.  0Higher numbers are more dense, default is
-density     : 1.4.


-m           : The margins of the page in cm.  Default is 1.
-margin:

-b           : The colour of a backing rectangle to place behind the text.
-background  : Colours are specified as "R,G,B" with each value between 0 and
             : 255.  "off" means no backing colour.  Default is "off".
     
-x           : The distance in cm that the backing rectangle should extend
-p           : from each edge of the text.  Default is 0.5cm.
-padding     :       
             
-n           : The title of the output to write as meta-data in the PostScript
-title       : file. Default is "Shanty output".
     
-l           : The orientation of the paper, can be "portrait", "landscape" or
-orientation : "auto". Default is "auto".

-rtl         : Switch to right-to-left text.

-btt         : Switch to bottom-to-top text.

-f           : Specify font.  The font name specified must be visible to the
-font        : PostScript viewer/distiller.  Specifying a font name here does
             : not just make a font magically appear.  Default is
	     : "Courier-Bold".

-altgd       : If you have problems loading the GD library, try this switch.


CREDIT
Many thanks to DFB (http://www.cs.nott.ac.uk/~dfb/) and contributors to 
comp.lang.postscript.
