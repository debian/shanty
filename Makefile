BIN_DIR=/usr/local/bin

all:
	@echo "Nothing to build, use "make install"

install:
	install -m 0755 shanty "${BIN_DIR}"
